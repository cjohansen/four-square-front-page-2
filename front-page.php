<?php 

get_header();

$args = array(
  'posts_per_page'      => 4,
  'post_type'           => 'newpage',
  'orderby'             => 'meta_value',
  'meta_key'            => 'position',
  'order'               => 'asc'
);
$query = new WP_Query( $args );
?>
<section class="page">
  <?php
  $a = 1;
  $billeder = "[";
  while ( $query->have_posts() ) {
    $query->the_post();  
    $billeder .= "'".get_the_post_thumbnail_url()."',";
  ?>
  <div id="box<?php echo $a; ?>" class="box">
    <div class="boxbox  box<?php echo $a; ?>">
      <p class="headline"><?php echo get_post_meta(get_the_ID(),"articletype",true); ?></p>
      <p class="content"><?php the_title(); ?></p>
      <p class="readmore">
        <?php if ( get_post_meta(get_the_ID(),"linktype",true) != "modal" ) { ?>
        <a href="<?php echo get_post_meta(get_the_ID(),"link",true); ?>">
          <?php echo get_post_meta(get_the_ID(),"readmore",true); ?>
        </a>
        <?php } else { ?>
        <a href="#" onclick="showModal(); ">
          <?php echo get_post_meta(get_the_ID(),"readmore",true); ?>
        </a>
        <?php } ?>
        <i class="fa fa-arrow-right" aria-hidden="true"></i>
      </p>
    </div>
  </div>
  <?php 
    $a++;
  } 
  $billeder .= "]";
  ?>
</section>

<section id="modal" class="modal">
  test test
</section>

<?php get_footer(); ?>

<script>
var billeder = <?php echo $billeder ?>;
for ( var a = 0; a < 4; a++ ) {
  document.getElementById('box'+(a+1)).style.backgroundImage = "url('"+billeder[a]+"')";
}

function showModal() {
  document.getElementById('modal').style.display = 'block';
}
</script>

